#include <stm32f10x_rcc.h>
#include <stm32f10x_gpio.h>

void Delay(void) {
	volatile uint32_t i;
	for (i=0; i != 0x80000; i++);
}

void _strcpy(char *s1,char *s2){
	while(*s1++=*s2++);
	return;
}

int main(void) {
	char testString[256];
	GPIO_InitTypeDef PORT;
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC , ENABLE);
	PORT.GPIO_Pin = (GPIO_Pin_8);
	PORT.GPIO_Mode = GPIO_Mode_Out_PP;
	PORT.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init( GPIOC , &PORT);

	_strcpy(testString,"Hello");

	for(;;) {
		GPIOC->ODR |= GPIO_Pin_8;
		Delay();
		 GPIOC->ODR &= ~GPIO_Pin_8;
		Delay();
	}

	_strcpy(testString,"Lera");

	return 0;
}
